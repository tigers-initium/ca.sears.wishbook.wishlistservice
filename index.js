const Express = require(`express`);
const ip = require(`ip`);
const http = require(`http`);
const bodyParser = require(`body-parser`);
const asynclib = require(`asyncawait/async`);
const compression = require(`compression`);
const helmet = require(`helmet`);
const winston = require(`winston`);
const winstonCouchDb = require(`winston-couchdb`).Couchdb;

const awaitlib = require(`asyncawait/await`);
const rp = require(`request-promise-native`);

asynclib(() => {
    const app = new Express();
    const logger = new winston.Logger({
        transports: [
            new (winston.transports.Console)(),
        ],
    });

    if (app.settings.env === `production`) {
        logger.add(winstonCouchDb, {
            db: `wishbook-logs`,
            level: `info`,
        });
    }

    // uncomment for redirecting to secure url by default
    // function ensureSecure(req, res, next){
    //   if(req.secure){
    //     // OK, continue
    //     return next();
    //   };
    //   res.redirect(`https://`+req.hostname+req.url);
    // };
    // app.all(`*`, ensureSecure);

    app.use(compression());
    app.use(helmet());

    app.use(bodyParser.json({
        limit: `50mb`,
    }));
    app.use(bodyParser.urlencoded({
        extended: true,
        limit: `50mb`,
    }));

    http.createServer(app).listen(8080, () => {
        logger.info(`Started server: http://${ip.address()}:8080`);
    });

    app.get(`/`, asynclib((req, res) => {
        try {
            const response = awaitlib(rp(`http://wishbookdb:5984/`));
            res.end(response);
        } catch (error) {
            logger.error(`${error.stack}`);
            res.end(new Error(`${error.message}`));
        }
    }));

    module.exports = app;
})();
